﻿using GreedKata;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GreedKataTests
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class Given_at_least_one_of_the_rolls_is_a_1
    {
        private ScoringService _scoringService;

        [TestInitialize]
        public void BeforeRunningTests()
        {
            _scoringService = new ScoringService();
        }

        [TestMethod]
        public void And_there_is_a_single_1_but_nothing_else_of_value_then_the_score_is_100()
        {
            //Act
            var score = _scoringService.Score(new[] { 1, 3, 4, 6, 2 });

            //Assert
            Assert.AreEqual(100, score);
        }

        [TestMethod]
        public void And_there_are_two_ones_then_the_score_is_200()
        {
            //Act
            var score = _scoringService.Score(new[] { 1, 3, 4, 1, 6 });

            //Assert
            Assert.AreEqual(200, score);
        }

        [TestMethod]
        public void And_there_are_three_ones_then_the_score_is_1000()
        {
            //Act
            var score = _scoringService.Score(new[] { 1, 3, 4, 1, 1 });

            //Assert
            Assert.AreEqual(1000, score);
        }

        [TestMethod]
        public void And_there_are_four_ones_then_the_score_is_1100()
        {
            //Act
            var score = _scoringService.Score(new[] { 1, 3, 1, 1, 1 });

            //Assert
            Assert.AreEqual(1100, score);
        }
    }
    // ReSharper restore InconsistentNaming
}
