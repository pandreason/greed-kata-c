﻿using System.IO;
using GreedKata;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GreedKataTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class Given_some_dice_rolls
    {
        private ScoringService _scoringService;

        [TestInitialize]
        public void BeforeRunningTests()
        {
            _scoringService = new ScoringService();
        }

        [TestMethod]
        public void And_the_there_are_no_triples_or_singles_of_value_then_the_score_is_0()
        {
            //Act
            var score = _scoringService.Score(new [] {2,3,4,6,2});

            //Assert
            Assert.AreEqual(0, score);
        }

        [TestMethod]
        public void And_there_is_a_single_5_but_nothing_else_of_value_then_the_score_is_50()
        {
            //Act
            var score = _scoringService.Score(new[] {2, 3, 4, 5, 6});

            //Assert
            Assert.AreEqual(50, score);
        }
    }
}
