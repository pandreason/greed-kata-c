﻿using System.Linq;

namespace GreedKata
{
    public class ScoringService
    {
        private const int SingleOneScore = 100;
        private const int SingleFiveScore = 50;
        private const int TripleOneScore = 1000;

        public object Score(int[] diceRolls)
        {
            var numberOfOnes = diceRolls.Count(x => x == 1);
            var numberOfFives = diceRolls.Count(x => x == 5);
            
            var scoreFromOnes = ((numberOfOnes % 3) * SingleOneScore) + ((numberOfOnes / 3) * TripleOneScore);

            return scoreFromOnes + (numberOfFives * SingleFiveScore);
        }
    }
}
